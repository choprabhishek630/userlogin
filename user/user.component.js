angular.module('userModule')
    .component('userComponent',{
        templateUrl: 'user/user.template.html',
        controller: function userController(){
            pushData();
            this.properties=['email' , 'userName' , 'password' , 'accessToken'];
            this.users=abc;
            this.search='';
            this.pageSize=10;
            this.propertyName='';
            this.reverse=false;
            this.currentPage=0;
            this.index=-1;
            this.currentLength=this.users.length;
            this.userObject={email:'',userName:'',password:'',accessToken:'',index:0};
            this.userObject1={email:'',userName:'',password:'',accessToken:'',index:0};
            this.viewField=false;
            this.reset=function(){
                this.userObject.email=this.userObject.password=this.userObject.accessToken=this.userObject.userName='';
                this.userObject.index=-1;
            };
            this.reset1=function(){
                this.userObject1.email=this.userObject1.password=this.userObject1.accessToken=this.userObject1.userName='';
                this.userObject1.index=-1;
            };
            this.sortBy = function(propertyName) {
                this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
                this.propertyName = propertyName;
                this.currentPage=0;
            };
            this.totalPage = function(){
                return Math.ceil(this.currentLength/this.pageSize);
            };
            this.check=function(){
                if(this.currentPage>=this.totalPage()-1)
                return true;
                return false;
            };
            this.validate=function(){
                this.currentPage=Math.floor((prevPageSize*this.currentPage)/this.pageSize);
                prevPageSize=this.pageSize;
            };
            this.getLimit= function(){
                return Math.min( (this.currentPage+1)*this.pageSize , this.currentLength );
            };
            this.redirect=function(val){
                if(this.search==''){
                    this.currentPage=prevPage;
                    this.currentLength=prevLength;
                    this.propertyName=prevProperty;
                    flag=true;
                }
                else if(flag){
                    prevPage=this.currentPage;
                    prevLength=this.currentLength;
                    prevProperty=this.propertyName;
                    this.propertyName='';
                    this.currentPage=0;
                    this.currentLength=val;                                                                                           
                    flag=false;
                }
                else{
                    this.currentLength=val;
                    this.currentPage=0;
                    this.propertyName='';
                }
            };
            this.checkSearchBox=function(){
                if(this.search!='')
                return true;
                return false;
            };
            this.add=function(index){
                var current={email: '',userName: '', password: '', accessToken: '',index: 0 };
                current.email=this.userObject1.email;
                current.password=this.userObject1.password;
                current.accessToken=this.userObject1.accessToken;
                current.userName=this.userObject1.userName;
                current.index=index;
                this.users.push(current);
                this.reset1();
                this.viewField=false;
                if(this.checkSearchBox()){
                    if(current.email.toLowerCase().search(this.search.toLowerCase())>=0 || current.userName.toLowerCase().search(this.search.toLowerCase())>=0 || current.password.toLowerCase().search(this.search.toLowerCase())>=0 || current.accessToken.toLowerCase().search(this.search.toLowerCase())>=0 )
                    this.currentLength++;
                }
                else
                this.currentLength++;
            };
            this.cancel1=function(){
                this.viewField=false;
                this.reset1();
            };
            this.update=function(index){
                this.users[index].email=this.userObject.email;
                this.users[index].userName=this.userObject.userName;
                this.users[index].password=this.userObject.password;
                this.users[index].accessToken=this.userObject.accessToken;
                this.reset();
                this.index=-1;
            };
            this.edit=function(index){
                this.index=index;
                this.userObject.email=this.users[index].email;
                this.userObject.userName=this.users[index].userName;
                this.userObject.password=this.users[index].password;
                this.userObject.accessToken=this.users[index].accessToken;
            };
            this.delete=function(index){
                console.log(index);
                if (index > -1) {
                    this.users.splice(index, 1);
                }
                for(var i=index;i<this.users.length;i++){
                    this.users[i].index--;
                }
                this.currentLength--;
                if(this.currentPage>=this.totalPage())
                this.currentPage--;
            };
            this.cancel=function(){
                this.index=-1;
                this.reset();
            };
        }
    });