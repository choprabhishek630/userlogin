angular.module('loginModule')
    .component('loginComponent',{
        templateUrl: 'login/login.template.html',
        controller: function loginController($location,$http){
            this.pwd="";
            this.email="";
            this.register = function() {
 			    $http({
  				    method: 'POST',
  				    url: 'https://alpha-api.circlein.io/api/user/userLogin',
  				    data: {email: this.email, password: this.pwd}
			    }).then(function successCallback(response) {
			    	alert("successfully Logged in");
					$location.path('/user');
	  		    }, function errorCallback(response) {
  			    	alert('incorrect username or password');
  			    });
 		    }
        }
    });