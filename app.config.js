angular.module('main')
    .config([ '$locationProvider' , '$routeProvider' , function config($locationProvider,$routeProvider){
        $locationProvider.hashPrefix('!');
        $routeProvider.
        when('/login',{
            template: '<login-component></login-component>'
        }).
        when('/user',{
            template: '<user-component></user-component>'
        }).
        otherwise('/login');

    } ]);